//
//  ViewController.swift
//  Santa Scanner
//
//  Created by Jeff Harmon on 12/19/22.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var scanSound:AVAudioPlayer?
    var clickSound:AVAudioPlayer?
    var naughtySound:AVAudioPlayer?
    var niceSound:AVAudioPlayer?
    @IBOutlet weak var scannerBackground: UIImageView!
    @IBOutlet weak var candyCaneNeedle: UIImageView!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var scanButtonLights: UIImageView!
    @IBOutlet weak var candyCaneCover: UIImageView!
    @IBOutlet weak var upperLights: UIImageView!
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    var scanButtonWidth = 0.0
    var scanButtonXOffset = 0.0
    var naughtyScanButtonPoint:CGFloat = 0.0
    var niceScanButtonPoint:CGFloat = 0.0
    var naughtyNiceWidth:CGFloat = 0.0
    var scanning = false
    var finishTimer = Timer()
    var scanTimer = Timer()
    var randomDouble = Double()
    var scanScore = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanButton.alpha = 1
        scanButton.backgroundColor = UIColor.clear
        scanButtonLights.alpha = 0
        upperLights.alpha = 0
        
        // Calculate width and height multipliers that need to be
        // used with button/image sizes to adjust for aspect ratio
        // on different devices
        let widthMultiplier = self.view.frame.size.width / 430
        let heightMultiplier = self.view.frame.size.height / 932
        
        // scaleFactor will be the multiplier that gets used
        // depending if the width or height is the smaller
        // scaled aspect ratio. Default to width then change
        // to height if height is the smaller scaleFactor
        var scaleFactor = widthMultiplier

        // If the aspect ratio means the scale factor for
        // width or height is smaller than the original
        // aspect ratio, need to calculate a buffer on the
        // dimension that has the smaller scale factor to
        // compensate for more pixels "buffering" that
        // dimensions. Init to zero and calculate the buffer
        // needed after determining which dimension has the
        // smaller scale factor
        var widthBuffer:Double = 0.0
        var heightBuffer:Double = 0.0

        // Assumed the width is the smaller scale factor,
        // but need to test it now.  If the height dimensions
        // has the smaller scale factor then we have to switch
        // to using the height aspect ratio for the scale factor
        // for all size calculations and compute the buffer for
        // the width so that x-offset math is right
        if (widthMultiplier > heightMultiplier) {
            scaleFactor = heightMultiplier
            widthBuffer = round( (self.view.frame.size.width - ( 430 * heightMultiplier) ) / 2 )
        } else {
            heightBuffer = round( (self.view.frame.size.height - ( 932 * widthMultiplier) ) / 2 )
        }
        
        // With the scale factor and buffers calculated, time to find
        // the dimmensions and offsets for the scan button
        let scanButtonYOffset = heightBuffer + round(round(1518/3) * scaleFactor)
        scanButtonXOffset = widthBuffer + round(round(82/3) * scaleFactor)
        scanButtonWidth = round(round(1117/3) * scaleFactor)
        let randomScoreWidth = round(round(234/3) * scaleFactor)
        let scanButtonHeight = round(round(239/3) * scaleFactor)
        
        // Calculate the positions of the naugty score, nice score, and random score
        // locations of the scan button to match the aspect ratio of the device
        naughtyNiceWidth = round( (scanButtonWidth - randomScoreWidth) / 2)
        naughtyScanButtonPoint = scanButtonXOffset + naughtyNiceWidth
        niceScanButtonPoint = naughtyScanButtonPoint + randomScoreWidth

        // With the dimensions and offsets calculated, change the frame
        // of the scan button to use them
        scanButton.frame = CGRect(x:scanButtonXOffset, y:scanButtonYOffset, width:scanButtonWidth, height:scanButtonHeight)
        
        // The scan button lights needs the same dimensions and offsets
        scanButtonLights.frame = CGRect(x:scanButtonXOffset, y:scanButtonYOffset, width:scanButtonWidth, height: scanButtonHeight)
        
        // Compute the dimensions and offsets for the candy cane
        // needle. Of special note, the y offset needs to have
        // half the height of the needle added to it because the
        // candy cane animation is going to rotate around the bottom
        // of the candy cane needle instead of around the middle
        // The y offset needs to compensate for that change
        let ccNeedleWidth = round(round(57/3) * scaleFactor)
        let ccNeedleHeight = round(round(603/3) * scaleFactor)
        let ccNeedleYOffset = heightBuffer + round( round(247/3) * scaleFactor ) + ccNeedleHeight/2
        let ccNeedleXOffset = widthBuffer + round( round(616/3) * scaleFactor )
        
        // Apply the calculated dimensions and offsets to the candy cane
        // needle
        candyCaneNeedle.frame = CGRect(x: ccNeedleXOffset, y: ccNeedleYOffset, width: ccNeedleWidth, height: ccNeedleHeight)
        // Set the candy cane needle graphic to rotate around
        // the bottom center of the graphic instead of the default
        // which is (0.5,0,5) or middle-middle
        candyCaneNeedle.layer.anchorPoint = CGPointMake(0.5, 1)
        
        // Get the address of all the music files ready to configure the
        // players
        let clickWavFile = Bundle.main.path(forResource: "click", ofType: "wav")
        let scanWavFile = Bundle.main.path(forResource: "scanning", ofType: "wav")
        let naughtyWavFile = Bundle.main.path(forResource: "naughtySound", ofType: "wav")
        let niceWavFile = Bundle.main.path(forResource: "niceSound", ofType: "wav")
        
        // Configure the media players so that they are ready to play the various
        // sounds.
        do {
            clickSound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: clickWavFile! ))
            scanSound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: scanWavFile! ))
            naughtySound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: naughtyWavFile! ))
            niceSound = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: niceWavFile! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch{
            print("Unable to load up scanning sound")
            print(error)
        }
    }
    
    // Animate the needle so that it will go from center and rotate
    // right first, then left. Using recursion and the completion event
    // on the animate method to control the timing of each rotate
    // animation
    func animateNeedle(right:Bool,final:Double,quantity:Int) {
        var radians:CGFloat
        
        // Check to see if the scan button got pressed while the
        // animation is running and stop the animation recursion if
        // it did.
        if (!self.scanning) {
            return
        }
        
        // If we have done all of the right and left animations
        // set the needle to the final spot determined by the
        // scanScore that was calculated at the press of the
        // scan button and passed in as the "final" parameter
        if (quantity == 0) {
            radians = CGFloat(final * CGFloat.pi)
            UIView.animate(withDuration:1.5, animations: {
                self.candyCaneNeedle.transform = CGAffineTransform(rotationAngle: radians )
            })
            return
        }
        
        // If the current animation is right, calculate the radians for
        // rotating to the right. Otherwise calculate the radians for
        // rotating to the left.
        if (right) { radians = CGFloat( 0.26 * CGFloat.pi) }
        else { radians = CGFloat( -0.26 * CGFloat.pi)}
        
        // Run the animation over a 1.5 second period. On completion,
        // make a recursive call of this function changing the parameters
        // as needed for the next part of the animation
        UIView.animate(withDuration:1.5, animations: {
            self.candyCaneNeedle.transform = CGAffineTransform(rotationAngle: radians )
        }, completion: {(value:Bool) in
            self.animateNeedle(right:!right, final:final, quantity: quantity-1)
        })
    }
    
    // Timer function to start the scan
    func startScan(timer:Timer) {
        // Play the scanning sound
        self.scanSound?.play()
        // Start the scanning animation. Rotate the candy cane needle to the
        // right first, provide the final spot for the candy cane needle,
        // and specify that the animation goes right and left 6 times
        self.animateNeedle(right: true, final: self.scanScore, quantity: 6)
    }
    
    // Timer function to finish the scan
    func finishScan(timer:Timer) {
        // Set the scanning flag to false so that everything knows
        // the scan is done
        self.scanning = false
        // Turn off the lights on the scan button
        self.scanButtonLights.alpha = 0
        // If the scanScore computed when the button was pressed is a "naughty"
        // result, play the naught sound and turn on the naughty lights
        if (self.scanScore < 0) {
            self.naughtySound?.play()
            self.upperLights.image = UIImage(named:"naughtyLights.png")
            self.upperLights.alpha = 0.7
        } else {
            self.niceSound?.play()
            self.upperLights.image = UIImage(named:"niceLights.png")
            self.upperLights.alpha = 0.8
        }
    }
    
    // Code to execute when the scan button is tapped
    @IBAction func scanButtonTapped(_ sender: Any, forEvent event: UIEvent) {
        // Check to see if a scan is already in progress. If it is, then cancel
        // automation timers and stop playing any audio so that the scan is
        // canceled.
        if (scanning) {
            // Cancel the animation timers
            scanTimer.invalidate()
            finishTimer.invalidate()
            // Set the flag to tell anything running the scan has been canceled
            scanning = false
            // Check to see if the click sound is playing and stop it.
            if (clickSound!.isPlaying) {
                clickSound?.stop()
            }
            // Reset the click sound to the beginning so it is ready for
            // the next scan
            clickSound?.currentTime = 0.0
            // Check to see if the scan sound is playing and stop it.
            if (scanSound!.isPlaying) {
                scanSound?.stop()
            }
            // Reset the scan sound to the beginning so it is ready for
            // the next scan
            scanSound?.currentTime = 0.0
            // Turn off the scan button lights
            scanButtonLights.alpha = 0
            // Turn off the scan result lights
            upperLights.alpha = 0
            // Move the candy cane needle back to center
            UIView.animate(withDuration:1.0, animations: {
                self.candyCaneNeedle.transform = .identity
            })
            return
        }
        
        // Set flag that will tell everything to keep going as a scan is
        // running
        scanning = true
        
        // Get the location of the scan button press
        let touch = event.touches(for: scanButton)?.first
        let location = touch?.location(in: nil)
        if let x = location?.x {
            // If the button was pressed on the left side, calculate
            // how naughty the scanScore should be based on how far
            // to the left the button was pressed. If it was to the
            // right side, then how far to the right for the nice score
            // If it was in the middle, then calculate a random score
            if (x <= naughtyScanButtonPoint) {
                let adjustedX = x - scanButtonXOffset
                let naughtyPercentage = 1 - (adjustedX/naughtyNiceWidth)
                scanScore = naughtyPercentage * -0.26
            } else if (x >= niceScanButtonPoint) {
                let adjustedX = x - niceScanButtonPoint
                scanScore = ( adjustedX / naughtyNiceWidth ) * 0.26
            } else {
                randomDouble = Double.random(in: 0.01...0.26)
                let randomGB = Bool.random()
                if (randomGB) {randomDouble = randomDouble * -1}
                scanScore = randomDouble
            }
        } else {
            randomDouble = Double.random(in: 0.01...0.26)
            let randomGB = Bool.random()
            if (randomGB) { randomDouble = randomDouble * -1}
            scanScore = randomDouble
        }
        
        // Play the click sound
        clickSound?.play()
        // Turn off the upper lights that may be there from
        // a previous scan
        upperLights.alpha = 0
        // Turn on the scan button lights to show that a scan
        // is in progress
        scanButtonLights.alpha = 0.6
        // Move the candy cane needle to center
        UIView.animate(withDuration:1.0, animations: {
            self.candyCaneNeedle.transform = .identity
        })

        // Setup a timer to start the scanning animation and audio in 3 seconds.
        // 3 seconds gives the click audio enough time to play and for the needle
        // to rotate back to center
        scanTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: startScan)
        
        // Setup another timer to finish the scan after 12.5 seconds. That gives
        // enough time for the sound and needle animations to complete. Without this
        // timer the scanning animation and audio will never play as the code to do them
        // gets overriden immediately to do this.
        if (scanning) {
            finishTimer = Timer.scheduledTimer(withTimeInterval: 12.5, repeats: false, block: finishScan)
        }
    }
}

